const express = require('express');
const generateControllers = require('../../util/query');
const Organisation = require('./model');

const organisationController = generateControllers(Organisation);
const organisationRouter = express.Router();

organisationRouter
  .route('/')
  .get(organisationController.getAll)
  .post(organisationController.createOne);

organisationRouter
  .route('/:id')
  .get(organisationController.getOne)
  .put(organisationController.updateOne)
  .delete(organisationController.deleteOne);

module.exports = organisationRouter;
