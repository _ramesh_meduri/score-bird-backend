const mongoose = require('mongoose');

const SchoolSchema = new mongoose.Schema(
  {
    city: { type: String, require: true },
    date_modified: { type: Date, default: Date.now },
    email: { type: String, require: true },
    name: { type: String, require: true },
    phone: { type: String, require: true },
    primary_contact: { type: String, require: true },
    state: { type: String, require: true },
    org_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Organisation' }
  },
  { versionKey: false }
);

const School = mongoose.model('School', SchoolSchema);

module.exports = School;
