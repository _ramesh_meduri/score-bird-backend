const express = require('express');
const generateControllers = require('../../util/query');
const Group = require('./model');

const groupController = generateControllers(Group);
const groupRouter = express.Router();

groupRouter
  .route('/')
  .get(groupController.getAll)
  .post(groupController.createOne);

groupRouter
  .route('/:id')
  .get(groupController.getOne)
  .put(groupController.updateOne)
  .delete(groupController.deleteOne);

module.exports = groupRouter;
