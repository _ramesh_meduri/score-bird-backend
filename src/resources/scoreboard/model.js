const mongoose = require('mongoose');

const ScoreboardSchema = new mongoose.Schema(
  {
    date_added: { type: Date },
    name: { type: String },
    unique_id: { type: String }
  },
  { versionKey: false }
);

const Scoreboard = mongoose.model('Scoreboard', ScoreboardSchema);

module.exports = Scoreboard;
