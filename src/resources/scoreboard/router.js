const express = require('express');
const generateControllers = require('../../util/query');
const Scoreboard = require('./model');

const scoreboardController = generateControllers(Scoreboard);
const scoreboardRouter = express.Router();

scoreboardRouter
  .route('/')
  .get(scoreboardController.getAll)
  .post(scoreboardController.createOne);

scoreboardRouter
  .route('/:id')
  .get(scoreboardController.getOne)
  .put(scoreboardController.updateOne)
  .delete(scoreboardController.deleteOne);

module.exports = scoreboardRouter;
