const express = require('express');
const generateControllers = require('../../util/query');
const Device = require('./model');

const deviceController = generateControllers(Device);
const deviceRouter = express.Router();

deviceRouter
  .route('/')
  .get(deviceController.getAll)
  .post(deviceController.createOne);

deviceRouter
  .route('/:id')
  .get(deviceController.getOne)
  .put(deviceController.updateOne)
  .delete(deviceController.deleteOne);

module.exports = deviceRouter;
