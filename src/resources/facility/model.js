const mongoose = require('mongoose');

const FacilitySchema = new mongoose.Schema(
  {
    wifi_access: { type: String },
    created_by: { type: String },
    created_on: { type: Date },
    date_modified: { type: Date },
    device_id: { type: String },
    ethernet_access: { type: String },
    modified_by: { type: String },
    modified_on: { type: Date },
    name: { type: String },
    state: { type: String },
    status: { type: String },
    facility_type_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'FacilityType'
    },
    school_id: { type: mongoose.Schema.Types.ObjectId, ref: 'School' },
    scoreboard_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Scoreboard' },
    venue_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Venue' }
  },
  { versionKey: false }
);

const Facility = mongoose.model('Facility', FacilitySchema);

module.exports = Facility;
