const express = require('express');
const { isValidToken } = require('./middlewares');

const restRouter = express.Router();

const schoolRouter = require('../resources/school/router');
const deviceRouter = require('../resources/device/router');
const streemingDeviceRouter = require('../resources/streeming_device/router');
const teamRouter = require('../resources/team/router');
const venueRouter = require('../resources/venue/router');
const userRouter = require('../resources/user/router');
const organisationRouter = require('../resources/organisation/router');
const sportRouter = require('../resources/sport/router');
const stateRouter = require('../resources/state/router');
const gameRouter = require('../resources/game/router');
const groupRouter = require('../resources/group/router');
const facilityRouter = require('../resources/facility/router');
const facilityTypeRouter = require('../resources/facility_type/router');
const scoreboardRouter = require('../resources/scoreboard/router');
const scheduleRouter = require('../resources/schedule/router');

restRouter.use('/school', isValidToken, schoolRouter);
restRouter.use('/device', isValidToken, deviceRouter);
restRouter.use('/streemingDevice', isValidToken, streemingDeviceRouter);
restRouter.use('/team', isValidToken, teamRouter);
restRouter.use('/venue', isValidToken, venueRouter);
restRouter.use('/user', isValidToken, userRouter);
restRouter.use('/organisation', isValidToken, organisationRouter);
restRouter.use('/sport', isValidToken, sportRouter);
restRouter.use('/state', isValidToken, stateRouter);
restRouter.use('/game', isValidToken, gameRouter);
restRouter.use('/group', isValidToken, groupRouter);
restRouter.use('/facility', isValidToken, facilityRouter);
restRouter.use('/facilityType', isValidToken, facilityTypeRouter);
restRouter.use('/scoreboard', isValidToken, scoreboardRouter);
restRouter.use('/schedule', isValidToken, scheduleRouter);

module.exports = restRouter;
