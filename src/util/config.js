require('dotenv/config');

const env = process.env;

let pem = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnI1QFSfGtTyCzgckjNWO
kDqHRfsUlOg43bD7UXBwdJ5IPe9ps70pZWQVlHs9Xna/RyCes5qcIyVlq2eeaK70
lJ6adVimZ8BxVC07ysoGaKvJuZyfbXZa1wXOFXZwyYE18vxAL9Ho+AXwb77/UCVZ
o6EB0fmu3vb1qplEqTNRRSXfwF62vI2bRBRKNqcDPZs2S+T5/PAPL6F2PODbrHgZ
h3hrlTm4bjng4AFQ9KoXMluN3nVyC9i7a7r9rKheXyKqXgPvl9pRvd2OKFxODXSw
SxsIFiDpaV9ODFNcU8+CKRIRt6UHxU+rpc4rjdMvtr5CpKZXMKEv4T6f/y/GEtWf
3QIDAQAB
-----END PUBLIC KEY-----`;

module.exports = {
  currEnv: env.NODE_ENV || 'development',
  port: env.PORT || 5000,
  db: env.MONGO_URI,
  pem: pem
};
