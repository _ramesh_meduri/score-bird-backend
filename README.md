### Scorebird -- REST API Application
---

##### Backend Tech Stack
> Node  
> Express  
> MongoDB  
> Mongoose

##### Commands for Local Development
```sh
npm install
npm start
```

Server Starts Listening on http://localhost:2222